# Mini Platform Game
This is my sandbox while I'm learning and experimenting with the awesome open source game engine [Godot]. :)

![Editing in Godot](./godot_screenshot.png)

## Gameplay
Some gameplay after my first 3 hours of getting used to GDScript:
![Traveler move around and jump](./mini_start.gif)

## Features
In v.0.001
 * Player can move and jump, can hit walls
 * Android virtual gamepad via 'Gamepad' addon implemented

## Credit
 * Thanks to Godot's cool community and nice documentation they wrote. Really this seems enormous effort, respect!
 * Video lesons from [HeartBeast]
 * [OpenPixelProject] for the great art
 * Gamepad addon by [fiaful]


[Godot]: https://godotengine.org/
[HeartBeast]: https://www.youtube.com/user/uheartbeast/videos
[OpenPixelProject]: https://openpixelproject.itch.io/opp2017sprites
[fiaful]: https://github.com/fiaful/Gamepad
