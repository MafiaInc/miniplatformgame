extends KinematicBody2D

const UPRIGHT = Vector2(0, -1)
const GRAVITY = 20
const MAX_SPEED = 200
const ACCELERATION = 50
const JUMP_HEIGHT = -550
var motion = Vector2()
var touch_pos = Vector2()
var screen_bounds = Vector2()
enum DIGITAL_DIRECTIONS { UP, LEFT, DOWN, RIGHT, RELEASED }
var active_direction = DIGITAL_DIRECTIONS.RELEASED
var jump_pressed = false

func _ready():
	screen_bounds = get_viewport().get_visible_rect().size

func _physics_process(delta):
	motion.y += GRAVITY
	var friction = false

	if Input.is_action_pressed("ui_right") or active_direction == DIGITAL_DIRECTIONS.RIGHT:
		motion.x = min(motion.x + ACCELERATION, MAX_SPEED)
		$Sprite.flip_h = false
		$Sprite.play("Run")
	elif Input.is_action_pressed("ui_left") or active_direction == DIGITAL_DIRECTIONS.LEFT:
		motion.x = max(motion.x - ACCELERATION, -MAX_SPEED)
		$Sprite.flip_h = true
		$Sprite.play("Run")
	else:
		$Sprite.play("Idle")
		friction = true

	if is_on_floor():
		if Input.is_action_just_pressed("ui_up") or jump_pressed:
			motion.y = JUMP_HEIGHT
		if friction == true:
			motion.x = lerp(motion.x, 0, 0.2)
	else:
		if motion.y < 0:
			$Sprite.play("Jump")
		else:
			$Sprite.play("Fall")
		if friction == true:
			motion.x = lerp(motion.x, 0, 0.05)

	motion = move_and_slide(motion, UPRIGHT)

func gamepad_force_changed(current_force, sender):
	print ("Gamepad force: ", current_force, ", direction: ", sender.direction)
	if sender.direction.size() > 0:
		active_direction = sender.direction[0]
	else:
		active_direction = DIGITAL_DIRECTIONS.RELEASED

func gamepad_stick_released():
	active_direction = DIGITAL_DIRECTIONS.RELEASED

func A_GamepadButton_up(sender):
	jump_pressed = false

func A_GamepadButton_down(sender):
	jump_pressed = true
